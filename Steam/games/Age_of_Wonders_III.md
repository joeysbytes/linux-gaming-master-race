# Age of Wonders III

## RetroCube

Works fine on Ubuntu 20.04.

## Performance

[The Retro Cube](./systems/retroCube.md): Sluggish but very playable

## OS Notes

The game works fine on Ubuntu 20.04 based OS's. After that, some libraries are updated and incompatible. See below
for some solutions.

## Kubuntu 22.04 Fixes

### Part 1: Missing Libraries

[source](https://steamcommunity.com/app/226840/discussions/6/3767858814401324346/)

1. In the terminal, cd to the game directory: __*baseDir*/steamapps/common/AoW3__
1. Type: __ldd AoW3Launcher | grep "not found"__
   * You will see 2 libraries not found:
     * libssl.so.1.0.0
     * libcrypto.so.1.0.0
1. Go to this [security website](http://security.ubuntu.com/ubuntu/pool/main/o/openssl1.0/):
   * Download the following files:
     * libssl1.0.0_1.0.2n-1ubuntu5.12_amd64.deb
     * libssl1.0.0_1.0.2n-1ubuntu5.12_i386.deb
   * __Note:__ The "5.12" version in the filename may change to a newer value.
   * __Note:__ You probably only need the i386 version, but you might as well install both.
   * Install each file: __sudo dpkg -i *fileName*__

### Part 2: Deprecated Libraries

[source](https://steamcommunity.com/app/226840/discussions/6/3767858814401324346/)

1. In the terminal, cd to the game directory: __*baseDir*/steamapps/common/AoW3__
1. Try running the application manually: __./AoW3Launcher.sh__
1. If you see a message similar to the following (anything referencing pthread_yield.so), you need to complete the steps in this part:

   ```text
   ERROR: ld.so: object 'pthread_yield.so' from LD_PRELOAD cannot be preloaded (cannot open shared object file): ignored.
   This application failed to start because it could not find or load the Qt platform plugin "xcb".

   Available platform plugins are: xcb.

   Reinstalling the application may fix this problem.
   Aborted (core dumped)
   ```

1. Go to a temp folder, and enter this text in a file called pthread_yield.c:

   ```c
   extern int sched_yield();

   int pthread_yield()
   {
           return sched_yield();
   }
   ```

1. Make sure gcc-multilib is installed:

   ```bash
   sudo apt update
   sudo apt install gcc-multilib
   ```

1. Compile with this command:

   ```bash
   gcc -m32 -shared -fPIC pthread_yield.c -o pthread_yield.so
   ```

1. Copy pthread_yield.so to the game directory: __*baseDir*/steamapps/common/AoW3__
1. In the game directory, edit __AoW3Launcher.sh__
1. Insert this line of text towards the end of the script, before the exec statement:

   ```bash
   export LD_PRELOAD="pthread_yield.so"
   ```

### Part 3: Graphics Issues

[source](https://steamcommunity.com/app/226840/discussions/0/1630790443998834839/)

1. These are some of the issues noticed which indicate this part may help you:

   * White squares on top-right quadrant of screen.
   * No options displaying to actually do anything from the title screen.

1. Go to the launch properties of the game, and add this text:

   ```text
   MESA_GL_VERSION_OVERRIDE=4.5 %command%
   ```

## Manjaro

[source](https://steamcommunity.com/app/226840/discussions/0/1630790443998834839/)

This is untested by me, but I noticed this note about it.

```text
My guess is you are using steam-native aka steam_runtime=0.
Try 'steam_runtime=1 steam' withn the terminal, then start the game to be sure if this is indeed the case.
If it is, you can remove the steam-native package while keeping the steam-manjaro (on manjaro, at least).
```
