# Cities Skylines

## RetroCube

The game runs, but if too many DLCs are loaded, it does not have the resources to run.
Recommend running without any DLCs.

Add this to Launch Options:

```text
-noWorkshop -disableMods
```
