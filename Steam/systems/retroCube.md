# The Retro Cube

The Retro Cube's main purpose is to run lighter, older, and emulated games and systems.

## Hardware

| Component | Descripiton |
| --- | --- |
| Motherboard | ASRock H77M-ITX |
| CPU | Intel i5-3570K @ 3.4 GHz |
| RAM | 16GB @ 1600 MHz |
| Video | VisionTek Radeon HD 5450 2GB (AMD) |
| Monitors | Primary: ASUS VE247 - 24", 1920 x 1080 |
| Sound | Built-In Motherboard Audio |

## Software

| Software | Description |
| --- | --- |
| OS | Linux Mint Debian Edition 6 |
| Drivers | Linux defaults |
